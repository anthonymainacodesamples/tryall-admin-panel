# Tryall Admin Panel README #

Code repository by Anthony Maina and Asili Labs.

### Languages ###
Javascript and Python

### Quick summary 
This is the admin only panel viewable by the team at Asili Labs to manage and modify data visible to clients from the main product.
This admin panels allows modifying (CRUD operations) of all data available to a user.

### Front End ###
The front end is built entirely in react and bootstrap.

### Back End ###
The back end is in 2 parts: a node server and python server. The node server handles all operations between the admin panel and the Mongo database. The python server is responsible for querying the trials and sorting by unclean data to highlight what product needs to be worked on.


### Version ###
Hidden

### How do I get set up? ###

To get set up run npm run dev in the main directory.


