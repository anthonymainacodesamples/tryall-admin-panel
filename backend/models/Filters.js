/**
 * Created by AnthonyMaina on 1/19/18.
 */

const mongoose = require('mongoose');
const { Schema }  = mongoose;


const FilterSchema = new Schema({
    name: String
});

mongoose.model('filters', FilterSchema);