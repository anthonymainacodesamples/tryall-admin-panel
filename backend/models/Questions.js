/**
 * Created by AnthonyMaina on 1/14/18.
 */

const mongoose = require('mongoose');
const { Schema }  = mongoose;

const questionSchema = new Schema ({
    prompt: String,
    points:{type:Number},
    options:
        [
            {
                orderNum:{type:Number},
                response:{type:String},
                published:{type:Boolean},
                linkedCategories:[],
                linkedFilters:[]
            }
        ]

});

mongoose.model("onboarding_questions", questionSchema);