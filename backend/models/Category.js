/**
 * Created by AnthonyMaina on 1/14/18.
 */

const mongoose = require('mongoose');
const { Schema }  = mongoose;


const BroadCategorySchema = new Schema({
    id: Schema.Types.ObjectId,
    name: String,
    browse_icon:String,
    sub_categories: []
});

mongoose.model('broad-categories', BroadCategorySchema);