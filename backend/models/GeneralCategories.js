/**
 * Created by AnthonyMaina on 1/16/18.
 */

const mongoose = require('mongoose');
const { Schema }  = mongoose;


const GenCategorySchema = new Schema({
    value: String
});

mongoose.model('categories', GenCategorySchema);