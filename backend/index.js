/**
 * Created by AnthonyMaina on 1/14/18.
 */

//Const statements
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

//Require files to run
require('./models/Category');
require('./models/Questions');
require('./models/GeneralCategories');
require('./models/Trial');
require('./models/Filters');


//Mongoose to mongodb connection
mongoose.connect("mongodb://admin:FM9-SMY-wZe-kZw@174.129.68.131:27017/tryall");


//Declare express route handlers for oAuth process
const app = express();

//Make middleware available across the app
/** Make body parser middleware allow app wide POST,PUT,PATCH requests
 to make available body as req.body
 */
app.use(bodyParser.json());

//Require route files in app running
require('./routes/questionsRoutes')(app);
require('./routes/categoriesRoutes')(app);
require('./routes/trialRoutes')(app);
require('./routes/filterRoutes')(app);

//Listening port info
const PORT = process.env.PORT || 8000;
app.listen(PORT);
