/**
 * Created by AnthonyMaina on 1/16/18.
 */

const mongoose = require('mongoose');
const BroadCategory = mongoose.model('broad-categories');
const GenCategory = mongoose.model('categories');
const _ = require('lodash');


module.exports = (app) => {

    app.get('/backend/general/categories', async (req,res) => {

        try {
           const genCats =  await GenCategory.find({});
            const finalArr = genCats.map( ({ value }) => {
               return value;
            });
           res.send({categoriesList:finalArr});
        } catch(error) {
            res.status(404).send(error);
        }
    });

    app.delete('/backend/category/sub', async (req,res) => {
        const categoryName = req.query.subCatName;

        try {
            await GenCategory.findOneAndRemove({value: categoryName});
            res.send(true);
        } catch(error) {
            res.status(404).send(error);
        }

    });

    app.delete('/backend/category/option', async (req,res) => {
        const categoryID = req.query.catID;
        const item = req.query.item;
        await BroadCategory.findByIdAndUpdate(
            {
                _id: categoryID,
            }
            ,
            {
                $pull:{ sub_categories:  item  }
            }
        ).exec();
        res.send(true);


    });
    app.post('/backend/modify/subcats', async (req,res) => {
        const interimQObject = req.body.newSubCat;
        const objJSON = eval("(function(){return " + JSON.parse(interimQObject) + ";})()");


        const categoryID = objJSON.catID;
        const item = objJSON.subCatText;

        const categoriesItem = await GenCategory.findOne({value: item});

        if (categoriesItem === null) {
            const newItem = new GenCategory({value:item});
            try{
                await newItem.save();
                await BroadCategory.findByIdAndUpdate(
                    {
                        _id: categoryID,
                    },
                    {
                        $push:{ sub_categories:  item  }
                    }
                ).exec();
                res.send(true);
            } catch(err) {
                console.log("Failed to add new sub category", err);
            }
        } else {

            await BroadCategory.findByIdAndUpdate(
                {
                    _id: categoryID,
                }
                ,
                {
                    $push: {sub_categories: item}
                }
            ).exec();
            res.send(true);
        }

    });


    app.get('/backend/generate/categoryID', (req,res) => {
        const category = new BroadCategory();
        try{
            res.send({newID:category._id});
        } catch(err) {
            res.status(404).send(err);
        }

    });
    app.delete('/backend/remove/category', async (req,res) => {
        const categoryID = req.query.categoryID;
        await BroadCategory.findByIdAndRemove({_id: categoryID});
        res.send(true);
    });


    app.post('/backend/categories/modify', (req,res) => {
        const interimQObject = req.body.categoryObj;
        const objJSON = eval("(function(){return " + JSON.parse(interimQObject) + ";})()");

        const catID = objJSON.catID;
        const catName = objJSON.catName;
        const browseIcon = objJSON.browseIcon;
        // const published = objJSON.published;
        const subCats = objJSON.subCategories;


        BroadCategory.findByIdAndUpdate(catID,
            {
                name:catName,
                browse_icon:browseIcon,
                sub_categories:subCats
            },{
                upsert: true,
            },
        ).exec();
        res.send(true);
    });

    app.get('/backend/categories/:id', async (req,res) => {
        const idCategory = req.params.id;
        const categorySingle = await BroadCategory.find({ _id:idCategory});
        try {
            res.send(categorySingle);
        }catch(err) {
            res.status(404).send(err);
        }

    });

    app.get('/backend/categories', async (req, res) => {
        let broadCategories = await BroadCategory.find({});
        let sortedCategories = _.sortBy(broadCategories, ({ name }) => {
            return name;
        });
        try {
            res.send(sortedCategories);
        }catch(err) {
            res.status(404).send(err);
        }
    });


};