/**
 * Created by AnthonyMaina on 1/14/18.
 */

const mongoose = require('mongoose');
const _ = require('lodash');
const Question = mongoose.model('onboarding_questions');
const BroadCategory = mongoose.model('broad-categories');
const Filters = mongoose.model('filters');

module.exports = (app) => {


    app.delete('/backend/linked/response', async (req,res) => {
        const questionId = req.query.questionID;
        const optionId = req.query.optionId;
        const categoryID = req.query.catId;
        await Question.update(
            {
                '_id': questionId,
                'options._id':optionId
            }
            ,
            { $pull: {
                'options.$.linkedCategories': categoryID
            }
            }
        ).exec();
        res.send(true);


    });
    app.post('/backend/linked/response', async (req,res) => {
        const interimObject = req.body.linkObj;
        const objJSON = eval("(function(){return " + JSON.parse(interimObject) + ";})()");

        const questionId = objJSON.questionID;
        const optionId = objJSON.optionId;
        const categoryID = objJSON.catId;
        await Question.update(
            {
                '_id': questionId,
                'options._id':optionId
            }
            ,
            { $push: {
                'options.$.linkedCategories': categoryID
            }
            }
        ).exec();
        res.send(true);
    });
    app.get('/backend/retrieve/:questionID/:optionID',async  (req,res) => {
        const questionID = req.params.questionID;
        const optionID = req.params.optionID;

        const item  = await Question.findOne({ _id:questionID }).populate({ path: 'options', match: { _id: optionID } }).exec();

        const itemName = item.prompt;
        const itemArray = item.options;

        const itemOption = _.find(itemArray, (item) => {
            return item._id.toString() === optionID.toString();

        });
        const returnOptDetails = {
            name: itemName,
            option:itemOption
        };

        try{
            res.send(returnOptDetails);
        } catch(err) {
            res.status(404).send(err);
        }

    });
    app.get ('/backend/linked/:questionID/:optionID', async (req,res) => {
        const questionID = req.params.questionID;
        const optionID = req.params.optionID;
        const item  = await Question.findOne({ _id:questionID }).populate({ path: 'options', match: { _id: optionID } }).exec();
        const itemArray = item.options;
        const itemOption = _.find(itemArray, (item) => {
            return item._id.toString() === optionID.toString();

        });
        const intIds = itemOption.linkedCategories;
        const finalCategories = await BroadCategory.find({_id:
            {
                $in:intIds

            }
        });


        try{
            // console.log(finalCategories);
            res.send({value:finalCategories});
        } catch(err) {
            res.status(404).send(err);
        }

    });
    app.get ('/backend/filter_linked/:questionID/:optionID', async (req,res) => {
        const questionID = req.params.questionID;
        const optionID = req.params.optionID;
        const item  = await Question.findOne({ _id:questionID }).populate({ path: 'options', match: { _id: optionID } }).exec();
        const itemArray = item.options;
        const itemOption = _.find(itemArray, (item) => {
            return item._id.toString() === optionID.toString();

        });
        const intIds = itemOption.linkedFilters;
        const finalFilters = await Filters.find({_id:
            {
                $in:intIds

            }
        });

        try{
            // console.log(finalCategories);
            res.send({value:finalFilters});
        } catch(err) {
            res.status(404).send(err);
        }

    });

    app.post('/backend/filter_linked', async (req,res) => {
        const interimObject = req.body.linkObj;

        const finalObject =  JSON.parse(interimObject);


        const questionId = finalObject.questionID;
        const optionId = finalObject.optionId;
        const filterID = finalObject.filterId;

        await Question.update(
            {
                '_id': questionId,
                'options._id':optionId
            }
            ,
            { $push: {
                'options.$.linkedFilters': filterID
            }
            }
        ).exec();
        res.send(true);



    });
    app.delete('/backend/filter_linked', async (req,res) => {
        const questionId = req.query.questionID;
        const optionId = req.query.optionId;
        const filterID = req.query.filterId;

        await Question.update(
            {
                '_id': questionId,
                'options._id':optionId
            }
            ,
            { $pull: {
                'options.$.linkedFilters': filterID
            }
            }
        ).exec();
        res.send(true);


    });

    app.get('/backend/generate/questionID', (req,res) => {
        const question = new Question();
        try{
            res.send({newID:question._id});
        } catch(err) {
            res.status(404).send(err);
        }

    });
    app.delete('/backend/remove/question', async (req,res) => {
        const questionID = req.query.questionID;
        await Question.findByIdAndRemove( {_id: questionID} );
        res.send(true);
    });

    app.delete('/backend/remove/option',async (req,res) => {
        const questionID = req.query.questionID;
        const optionID = req.query.optionID;
        await Question.findByIdAndUpdate(
            {
                _id: questionID,
            }
            ,
            {
                $pull:{ options: { _id: optionID } }
            }
        ).exec();
        res.send(true);

    });

    app.post('/backend/modify/question', (req,res) => {
        const interimQObject = req.body.questionObj;
        const objJSON = eval("(function(){return " + JSON.parse(interimQObject) + ";})()");

        const questionID = objJSON.questionID;
        const questionVal = objJSON.questionVal;
        const questionPoints = objJSON.questionPoints;
        const published = objJSON.published;
        const questionOpts = objJSON.questionOpts;

        Question.findByIdAndUpdate(questionID,
            {
                prompt:questionVal,
                published:published,
                points:questionPoints,
                options:questionOpts
            },{
                upsert: true,
            },
        ).exec();
        res.send(true);
    });

    app.post('/backend/modify/options', (req,res) => {
        const interimObject = req.body.newResponse;
        const objJSON = eval("(function(){return " + JSON.parse(interimObject) + ";})()");

        const questionID = objJSON.questionID;
        const optionNum = objJSON.optionNum;
        const optionText = objJSON.optionText;
        console.log(objJSON);

        const newOption = {
            orderNum:optionNum,
            response:optionText,
            linkedCategories:[]
        };
        Question.findByIdAndUpdate(questionID,
            {
                $push:{"options":newOption}
            }
        ).exec();
        res.send(true);
    });

    app.get('/backend/questions/retrieve', async (req,res) => {

        const questions = await Question.find({});

        try {
            res.send(questions);
        }catch(err) {
            res.status(404).send(err);
        }
    });

    app.get('/backend/questions/:id', async (req,res) => {
        const idQuestion = req.params.id;
        const questionSingle = await Question.find({ _id:idQuestion});
        try {
            res.send(questionSingle);
        }catch(err) {
            res.status(404).send(err);
        }

    });
};