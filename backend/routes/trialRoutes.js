/**
 * Created by AnthonyMaina on 1/17/18.
 */

const mongoose = require('mongoose');
const Trial = mongoose.model('tryallcompanies');
const _ = require('lodash');

module.exports = (app) => {

    app.get('/backend/trial/filters/:trialID', async (req,res) => {

        // console.log(req.params.trialID);

        // const trialID = "59ebc295dd297a316cf69151";

        try {
            const filtersPerTrial =  await Trial.find({ _id: req.params.trialID }).select('-_id filters');
            const filtersArray = filtersPerTrial[0].filters;
            res.send({filtersList:filtersArray});
        } catch(error) {
            res.status(404).send(error);
        }
    });

    app.get('/backend/trial/newID', (req,res) => {
        const trial = new Trial();
        try{
            res.send({newID:trial._id});
        } catch(err) {
            res.status(404).send(err);
        }
    });




    app.post('/backend/create/trial', async (req,res) => {
        const interimQObject = req.body.newTrial;
        const objJson = JSON.parse(interimQObject);

        const finalArray = _.uniq(objJson.categories);
        const finalFilters = _.uniq(objJson.filters);

        const newTrial = new Trial({
            name:objJson.name,
            tagLine: objJson.tagline,
            description: objJson.desc,
            websiteLink:objJson.link ,
            freeTrialPage:objJson.freeTrialLink,
            DurationInDays:objJson.duration,
            conditionalDuration: objJson.conditionalDuration,
            monthlyDollarPrice: objJson.price,
            conditionalPricing:objJson.conditionalPricing ,
            requiresCreditCard:objJson.credit_card,
            logoLink:objJson.logoLink ,
            categories: finalArray,
            filters:finalFilters
        });

        try{
            await newTrial.save();
            res.send(true);
        } catch(err) {
            console.log("Failed to add new trial", err);
        }

    });

};