/**
 * Created by AnthonyMaina on 1/19/18.
 */

const mongoose = require('mongoose');
const Filter = mongoose.model('filters');
const _ = require('lodash');

module.exports = (app) => {

    app.post('/backend/filter/new', async (req,res) => {
        const interimObject = req.body.filterName;
        const objJson = JSON.parse(interimObject);
        const newFilter= new Filter({
            name:objJson.filterName
        });
        try{
            await newFilter.save();
            res.send(true);
        } catch(err) {
            console.log("Failed to add new filter", err);
        }
    });
    app.get('/backend/filters/list', async (req,res) => {
        try{
            const filtersList = await Filter.find({});
            res.send({filterItems:filtersList});
        } catch(err) {
            console.log("Failed to add new filter", err);
        }
    });


    app.post('/backend/modify/filter', async (req,res) => {
        const interimObject = req.body.filterItem;
        const objJson = JSON.parse(interimObject);

        try{
            await Filter.findByIdAndUpdate(
                {
                    _id: objJson.filterId,
                }
                ,
                {
                   name: objJson.filterName
                }
            ).exec();


            res.send(true);
        } catch(err) {
            console.log("Failed to add new filter", err);
        }
    });

    app.delete('/backend/remove/filter',async (req,res) => {

        const filterID = req.query.filterId;
        await Filter.findByIdAndRemove({ _id: filterID }).exec();
        res.send(true);

    });

};
