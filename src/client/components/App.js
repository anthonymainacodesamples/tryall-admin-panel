import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';
import Navbar from './Navigation/NavBar';
import Main from './Main';


class App extends Component {


  render() {
    return (
      <div className="App">
          <Navbar/>
          <Main/>

      </div>
    );
  }
}

export default App;
