/**
 * Created by AnthonyMaina on 1/16/18.
 */

import React, { Component } from 'react';
import axios from 'axios';

class SingleCategory extends Component {

    constructor(props) {
        super(props);
        this.toggleClass= this.toggleClass.bind(this);
        this.state = {
            active: this.props.isSelected,
        };
    }

    async linkCategory(id) {
        const currentCatId = id;
        const queId = this.props.parentQues;
        const optId = this.props.optId;

        const bodyContent = JSON.stringify({
            questionID:queId,
            catId: currentCatId,
            optionId:optId
        });

        const headers = {
            'Content-Type': 'application/json'
        };

        const data = {
            linkObj: JSON.stringify(bodyContent)
        };
        const res = await axios.post('/backend/linked/response',data, headers );
        if(res.data){
            window.location.reload()
        }else{
            console.log('error updating category');
        }

    }
    async unLinkCategory(id) {
        const currentCatId = id;
        const queId = this.props.parentQues;
        const optId = this.props.optId;

        const res = await axios.delete('/backend/linked/response',
            {
                params:
                    {
                        questionID:queId,
                        catId: currentCatId,
                        optionId:optId
                    }
            });

    }


    toggleClass(id) {
        const currentState = this.state.active;
        if(currentState === true) {
            this.unLinkCategory(id);
        } else {
            this.linkCategory(id);
        }
        this.setState({ active: !currentState });
    };




    render() {
        const { item } = this.props;

        return (
            <li className={ this.state.active  ? " Product productActive" : "Product"} key={item._id} onClick={() => this.toggleClass(item._id)}>
                <i  className={this.state.active  ? `Product-icon Product-Icon-Selected fa ${item.browse_icon}` :`Product-icon fa ${item.browse_icon}`}/>
                <h6 className={this.state.active ? "Product-name Product-Name-Selected" : "Product-name"}>
                    {item.name}
                </h6>
            </li>
        );
    }
}
export default SingleCategory;