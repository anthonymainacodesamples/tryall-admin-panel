/**
 * Created by AnthonyMaina on 1/14/18.
 */

import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import {PageHeader,Button, NavItem,Nav,MenuItem,FormGroup,InputGroup,FormControl,DropdownButton,Col,Panel } from 'react-bootstrap';
import axios from 'axios';
import _ from 'lodash';
import '../../styles/browseCategories.css';


class CategoryManagement extends Component {

    constructor(props){
        super(props);
        this.state = {
            value: [],
            randomID:"",
            broadCatsSelected:true,
            open: false,
            newOption:"",
            filters:[]
        };

        this.changeNew = this.changeNew.bind(this);
        this.change = this.change.bind(this);
    }

    componentWillMount() {
        this.fetchCategories();
        this.generateNewID();
        this.fetchFilters();

    }
    async fetchFilters() {
        const res = await axios.get('/backend/filters/list');
        this.setState({
            filters:res.data.filterItems
        });
    }
    async generateNewID() {
        const res = await axios.get('/backend/generate/categoryID');
        this.setState({
            randomID:res.data.newID
        });
    }

    async fetchCategories() {
        try {
            let response = await fetch('/backend/categories');
            const item = await response.json();
            this.setState({
                value: item
            });
            return item;
        } catch (error) {
            console.error(error);
        }

    }

    renderFiltersHeader(){
        return (
            <div>
            <PageHeader className="text-left">
                <small>Trial Filters </small>
                <div className="pull-right">
                        <Button bsStyle="primary" onClick={() => this.setState({ open: !this.state.open })}> Add Filter</Button>
                </div>
            </PageHeader>
                <Panel id="collapsible-panel-example-1" expanded={this.state.open} collapsible>
                    <FormGroup controlId="newFilter" validationState="warning">
                        <InputGroup>
                            <FormControl type="text" value={this.state.newOption} onChange={this.changeNew} validationState={this.state.newOption? "success":"warning"} />
                            <InputGroup.Button>
                                <Button bsStyle="warning" type="submit"onClick={this.handleNewFormSubmit} > Add To Database </Button>
                            </InputGroup.Button>
                        </InputGroup>
                    </FormGroup>
                </Panel>
            </div>

        );
    }
    renderHeader() {
        return (
            <PageHeader className="text-left">
                <small>Official Broad Categories </small>
                <div className="pull-right">
                    <Link to={`/categories/edit/${this.state.randomID}`} style={{ textDecoration: 'none' }}>
                        <Button bsStyle="primary"> Add Category</Button>
                    </Link>
                </div>
            </PageHeader>
        );
    }
    renderIndividualCategory() {
        return _.map(this.state.value, item => {
            return(
                <li className="Product" key={item._id}>
                    <Link to={`/categories/edit/${item._id}`}  style={{ textDecoration: 'none' }}>
                        <i  className={`Product-icon fa ${item.browse_icon}`}/>
                        <h6 className="Product-name">
                            {item.name}
                        </h6>
                    </Link>
                </li>
            );
        })
    }
    renderListOfCategories() {
        return (
            <div>
                <ul className="ProductList">
                    {this.renderIndividualCategory()}
                </ul>
            </div>
        );
    }
    renderNavigationForCategoryTypes() {
        return (
            <Nav
                bsStyle="pills"
                justified
                activeKey={this.state.broadCatsSelected ? 1 : 2}
                onSelect={(key) => this.toggleView(key)}
            >
                <NavItem eventKey={1} title="BroadCats">
                    <strong>Broad Categories</strong>
                </NavItem>
                <NavItem eventKey={2} title="CatFilters">
                    <strong>Trial Filters</strong>
                </NavItem>
            </Nav>
        );
    }
    renderFiltersList() {
        return (
            <Col xs={12} md={11}>
                {this.renderIndividualFilters()}
            </Col>
        );
    }
    renderIndividualFilters() {
        return _.map(this.state.filters, item => {
            return(
                <form>
                    <FormGroup controlId={item._id}>
                        <InputGroup>
                            <FormControl type="text" value={item.name}  onChange={this.change} />
                            <DropdownButton
                                componentClass={InputGroup.Button}
                                id="input-dropdown-addon"
                                title="Options">
                                <MenuItem key="1" type="submit" onSelect={() => this.handleFilterUpdate(item._id)}>Update</MenuItem>
                                <MenuItem divider />
                                <MenuItem key="2" onSelect={() => this.handleFilterDelete(item._id)} >Delete Filter</MenuItem>
                            </DropdownButton>
                        </InputGroup>
                    </FormGroup>
                </form>
            );
        })
    }

    async handleFilterUpdate(id) {
        let newCopy = this.state.filters;

        // Find item index using _.findIndex
        var index = _.findIndex(newCopy, {_id: id});

        const body = JSON.stringify({
            filterId:id,
            filterName:newCopy[index].name
        });

        const headers = {
            'Content-Type': 'application/json'
        };

        const data = {
            filterItem: body
        };
        const res = await axios.post('/backend/modify/filter',data, headers );
        if(res.data){
            this.fetchFilters();
        }else{
            console.log('error updating category');
        }

    };

    async handleFilterDelete(id) {
        const res = await axios.delete('/backend/remove/filter',
            {
                params:
                    {
                        filterId:id
                    }
            });
        if(res.data){
            this.fetchFilters();
        }else{
            console.log('error updating question');
        }
    }

    change = (e) => {
        let id = e.target.id;
        let newCopy = this.state.filters;
        let newArr = _.map(newCopy, function(a) {
            return a._id === id ? {_id: id, name: e.target.value } : a;
        });

        this.setState({
            filters: newArr
        });
    };
    changeNew = (e) => {
        let id = e.target.id;
        switch (id){
            case 'newFilter':
                this.setState({
                    newOption: e.target.value
                });
                break;
            default:
                console.log('in default case');
                break;
        }
    };
    handleNewFormSubmit = (e) => {
        e.preventDefault();
        const newItem = this.state.newOption;

        if(newItem) {
            const body = JSON.stringify({
                filterName: newItem
            });
            this.appendFilter(body);
        }
    };

    async appendFilter(item) {
        const headers = {
            'Content-Type': 'application/json'
        };

        const data = {
            filterName: item
        };
        const res = await axios.post('/backend/filter/new',data, headers );
        if(res.data){
            this.setState({
                newOption:""
            });
            this.setState({ open: !this.state.open });
            this.fetchFilters();
        }else{
            console.log('error updating category');
        }

    }

    renderFilters() {
        return (
          <div className="text-left">
              {this.renderFiltersHeader()}
              {this.renderFiltersList()}
          </div>
        );
    }
    toggleView(key) {
        const displayBroadCats = this.state.broadCatsSelected;
        if(key === 1 && displayBroadCats===false) {
            this.setState({
                broadCatsSelected: true
            });
        } else  if(key === 2 && displayBroadCats===true){
            this.setState({
                broadCatsSelected: false
            });
        }
    }


    renderCorrectPage() {
        const displayBroadCats = this.state.broadCatsSelected;

        if(displayBroadCats) {
            return (
                <div>
                    {this.renderHeader()}
                    {this.renderListOfCategories()}
                </div>
        );

        } else {
            return (
                <div>
                    {this.renderFilters()}
                </div>
            );
        }
    }

    render() {
        return  (
            <div className="container">
                {this.renderNavigationForCategoryTypes()}
                {this.renderCorrectPage()}

            </div>
        );
    }
}
export default CategoryManagement;