/**
 * Created by AnthonyMaina on 1/16/18.
 */


import React,{ Component } from 'react';
import { PageHeader,Button } from 'react-bootstrap';
import _ from 'lodash';
import SingleCategory from './SingleCategory';



class CategorySelectionPerOption extends Component {

    constructor(props) {
        super(props);
        this.state = {
            questionID:"",
            optID:"",
            questionVal:"",
            questionOpt:{},
            completeMain:false,
            completeSelected:false,
            value:{},
            valueCurrent:{}
        };
    }

    componentDidMount() {
        const { questionID,optionID } = this.props.match.params;

        this.getQuestionInfo(questionID,optionID);
        this.fetchCategories();
        this.fetchCurrentSelected(questionID,optionID);

    }
    async fetchCategories() {
        try {
            let response = await fetch('/backend/categories');
            const item = await response.json();
            this.setState({
                completeMain:true,
                value: _.mapKeys(item, '_id')
            });
            return item;
        } catch (error) {
            console.error(error);
        }

    }
    async fetchCurrentSelected(qId,oId) {
        try {
            let response = await fetch(`/backend/linked/${qId}/${oId}`);
            const item = await response.json();
            this.setState({
                completeSelected:true,
                valueCurrent: _.mapKeys(item.value, '_id')
            });

            return item;
        } catch (error) {
            console.error(error);
        }

    }
    async getQuestionInfo(qID,oID)  {
        this.setState({
            questionID:qID,
            optionID: oID
        });

        try {
            let response = await fetch(`/backend/retrieve/${qID}/${oID}`);
            const item = await response.json();
            if(item) {
                this.setState({
                    questionVal: item.name,
                    questionOpt: item.option
                });
            }
            return item;
        } catch (error) {
            console.error(error);
        }
    }

    renderHeader() {

        const currentOpt = this.state.questionOpt;
        return (
            <PageHeader className="text-left">
                <small>Question: {this.state.questionVal}</small>
                <br />
                <small>Option: { currentOpt.response }</small>

            </PageHeader>
        );
    }

    renderCategories() {
        const totalCat = this.state.value;
        const currentCat = this.state.valueCurrent;

        if(this.state.completeMain && this.state.completeSelected) {
            return _.map(totalCat, item => {
                if (currentCat[item._id]) {
                    return (
                        <SingleCategory
                            key={item._id}
                            item={item}
                            isSelected={true}
                            parentQues={this.state.questionID}
                            optId={this.props.match.params.optionID}
                        />
                    );
                } else {
                    return (
                        <SingleCategory
                            key={item._id}
                            item={item}
                            isSelected={false}
                            parentQues={this.state.questionID}
                            optId={this.props.match.params.optionID}/>
                    );
                }
            })
        }
    }



    render() {
        return (
          <div className="container">
              {this.renderHeader()}
              <ul className="ProductList">
              {this.renderCategories()}
              </ul>
          </div>
        );
    }

}
export default CategorySelectionPerOption;