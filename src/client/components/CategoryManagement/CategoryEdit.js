/**
 * Created by AnthonyMaina on 1/16/18.
 */


import React, { Component } from 'react';
import {Link } from 'react-router-dom';
import axios from 'axios';
import _ from 'lodash';
import { PageHeader, Form, FormGroup, FormControl,ControlLabel,Col,HelpBlock,Button  } from 'react-bootstrap'

class CategoryEdit extends Component {


    constructor(props){
        super(props);
        this.state = {
            catID:"",
            catName:"",
            browseIcon:"",
            subCategories:[],
            questionPrompt:"success",
            newOption:"",
            publishedState:false
        };
        this.change = this.change.bind(this);
        this.deleteCategory = this.deleteCategory.bind(this);
        this.changeNew = this.changeNew.bind(this);
        this.deleteSubCat = this.deleteSubCat.bind(this);
        this.removeFromDB = this.removeFromDB.bind(this);
    }
    change(e) {
        let id = e.target.id;
        switch (id){
            case 'categoryPrompt':
                this.setState({
                    catName: e.target.value
                });
                break;
            case 'browseIcon':
                this.setState({
                    browseIcon: e.target.value
                });
                break;
            default:
                this.updateSpecificSubCat(id,e);
                break;
        }
    }

    changeNew(e) {
        let id = e.target.id;
        switch (id){
            case 'newCat':
                this.setState({
                    newOption: e.target.value
                });
                break;
            default:
                console.log('in default case');
                break;
        }
    }

    handleNewFormSubmit = (e) => {
        e.preventDefault();

        const body = JSON.stringify({
            catID:this.state.catID,
            subCatText: this.state.newOption
        });
        this.appendCategory(body);
    };


    async appendCategory(bodyContent) {
        const headers = {
            'Content-Type': 'application/json'
        };

        const data = {
            newSubCat: JSON.stringify(bodyContent)
        };
        const res = await axios.post('/backend/modify/subcats',data, headers );
        if(res.data){
            const { id } = this.props.match.params;
            this.getCategoryInfoFromApi(id);
            this.setState({
                newOption:""
            })
        }else{
            console.log('error updating category');
        }

    }


    updateSpecificSubCat(ind,e) {
        const indNum = Number.parseInt(ind);
        const newSubCatsArray = this.state.subCategories;
        newSubCatsArray[indNum] = e.target.value;
        this.setState({
            subCategories:newSubCatsArray
        });

    }
    componentDidMount() {
        const { id } = this.props.match.params;
        this.getCategoryInfoFromApi(id);
    }
    async getCategoryInfoFromApi(id)  {
        this.setState({
            catID:id
        });

        try {
            let response = await fetch(`/backend/categories/${id}`);
            const item = await response.json();
            const obj = item[0];
            if(obj) {
                this.setState({
                    catName: obj.name,
                    browseIcon: obj.browse_icon,
                    subCategories: obj.sub_categories
                });
            }
            return item;
        } catch (error) {
            console.error(error);
        }
    }
    renderForm() {

        return (
            <form>
                <FormGroup className="text-left" controlId="categoryPrompt" validationState={this.state.catName? "success":"error"}>
                    <ControlLabel>Category Name</ControlLabel>
                    <FormControl type="text" value={this.state.catName} onChange={this.change} />
                        <HelpBlock>If this is a new category, type the name here then click update category above before adding sub-categories.</HelpBlock>
                    <FormControl.Feedback />
                </FormGroup>
                <FormGroup className="text-left" controlId="browseIcon" validationState={this.state.browseIcon? "success":"error"}>
                    <ControlLabel>Icon</ControlLabel>
                    <FormControl type="text" value={this.state.browseIcon} onChange={this.change} />
                    <HelpBlock>For this to show up on the site, it has to be a valid font awesome icon. All icons can be found here: <a href="http://fontawesome.io/icons/" target="_blank" rel="noopener noreferrer" >Icons</a></HelpBlock>
                    <FormControl.Feedback />
                </FormGroup>
                <Form componentClass="fieldset" horizontal>
                    {this.renderOptionsSubCategories()}
                </Form>
            </form>
        );
    }

    renderNew() {
        return (
            <Form componentClass="fieldset" horizontal>
                <FormGroup controlId="newCat" validationState="warning">
                    <Col componentClass={ControlLabel} xs={2}>
                        New Sub-Category
                    </Col>
                    <Col xs={4}>
                        <FormControl type="text" value={this.state.newOption} onChange={this.changeNew} validationState={this.state.newOption? "success":"warning"} />
                        <FormControl.Feedback />
                        <HelpBlock>This directly adds a new sub-category </HelpBlock>
                    </Col>
                    <Col xs={6}>
                        <Button type="submit"onClick={this.handleNewFormSubmit}  > Add New Sub-Category</Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }

    renderHeader() {
        return (
            <PageHeader className="text-left">
                <small>Category ID: {this.state.catID} </small>
                <div className="pull-right">

                    <Link to={'/categories'} style={{ textDecoration: 'none' }}>
                    <Button bsStyle="danger" onClick={() => this.deleteCategory()}>  Delete Category</Button>
                    </Link>
                    {'  '}
                    <Button bsStyle="primary" type="submit" onClick={this.handleFormSubmit}> Update Category</Button>
                </div>

            </PageHeader>
        );
    }

    async deleteSubCat(catID,item) {
        const res = await axios.delete('/backend/category/option',
            {
                params:
                    {
                        catID,
                        item
                    }
            });
        if(res.data){
            const { id } = this.props.match.params;
            this.getCategoryInfoFromApi(id);
        }else{
            console.log('error updating question');
        }


    }
    async removeFromDB(catID,item) {
         await axios.delete('/backend/category/option',
            {
                params:
                    {
                        catID,
                        item
                    }
            });

        const res = await axios.delete('/backend/category/sub',
            {
                params:
                    {
                        subCatName:item
                    }
            });
        if(res.data){
            const { id } = this.props.match.params;
            this.getCategoryInfoFromApi(id);
        }else{
            console.log('error updating question');
        }


    }

    renderOptionsSubCategories() {
        const itemsArray = this.state.subCategories;
        return _.map(itemsArray, item => {
            const currentInd = itemsArray.indexOf(item);
            return (
                <FormGroup controlId={`${currentInd}`} key={currentInd} validationState={ "success"}>
                    <Col componentClass={ControlLabel} xs={2}>
                        Sub Category {currentInd}
                    </Col>
                    <Col xs={4}>
                        <FormControl type="text" value={item} onChange={this.change} />
                        <FormControl.Feedback />
                    </Col>
                    <Col xs={6}>
                        <Button onClick={() => this.deleteSubCat(this.state.catID,item)}> Remove Sub-Category</Button>
                        {"   "}
                        <Button bsStyle="danger" onClick={() => this.removeFromDB(this.state.catID,item)}> Delete From Database</Button>
                    </Col>
                </FormGroup>
            );
        });
    }
    handleFormSubmit = (e) => {
        e.preventDefault();
        const body = JSON.stringify({
            catID:this.state.catID,
            catName: this.state.catName,
            browseIcon:this.state.browseIcon,
            subCategories: this.state.subCategories
        });
        this.updateCategory(body);
    };

    async deleteCategory() {
        const res = await axios.delete('/backend/remove/category',
            {
                params:
                    {
                        categoryID: this.state.catID
                    }
            });
        if(res.data){
            const { id } = this.props.match.params;
            this.getCategoryInfoFromApi(id);
        }else{
            console.log('error updating question');
        }
    }

    async updateCategory(bodyContent) {
        console.log(bodyContent);
        const headers = {
            'Content-Type': 'application/json'
        };

        const data = {
            categoryObj: JSON.stringify(bodyContent)
        };
        const res = await axios.post('/backend/categories/modify',data, headers );
        if(res.data){
            window.location.reload()
        }else{
            console.log('error updating category');
        }

    }


    render(){
        return (
          <div className="container">
              {this.renderHeader()}
              {this.renderForm()}
              {this.renderNew()}
          </div>
        );
    }
}
export default CategoryEdit;