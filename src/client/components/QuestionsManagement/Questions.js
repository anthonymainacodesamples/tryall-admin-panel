/**
 * Created by AnthonyMaina on 1/14/18.
 */

import React, { Component } from 'react';
import { PageHeader, Button} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import _ from 'lodash';
import axios from 'axios';
// Custom Components
import SingleQuestion from './SingleQuestion';

class Questions extends Component {

    constructor(props){
        super(props);
        this.state = {
            value: [],
            randomID:""
        }


    }

    componentDidMount(){
        this.getQuestionsFromApi();
        this.generateNewID();
    }

    async getQuestionsFromApi()  {
        try {
            let response = await fetch('/backend/questions/retrieve');
            const item = await response.json();
            this.setState({
                value: item
            });
            return item;
        } catch (error) {
            console.error(error);
        }
    }

    async generateNewID() {
        const res = await axios.get('/backend/generate/questionID');
        this.setState({
            randomID:res.data.newID
        });
    }

    renderHeader() {
        return (
            <PageHeader className="text-left" >
                <small>On-boarding Questions Manager </small>
                <div className="pull-right">
                    <Link to={`/questions/edit/${this.state.randomID}`} style={{ textDecoration: 'none' }}>
                        <Button bsStyle="primary"> Add Question</Button>
                    </Link>
                </div>
            </PageHeader>
        );
    }


    renderQuestions() {
       return _.map(this.state.value, item => {
           return (
               <SingleQuestion questionID={item._id} questionPrompt={item.prompt} key={item._id} answersList={item.options}/>
           );
       });
    }

    render() {
        return (
          <div className="container">
              {this.renderHeader()}
              {this.renderQuestions()}
          </div>
        );
    }
}

export default Questions;