/**
 * Created by AnthonyMaina on 1/22/18.
 */


import React, { Component } from 'react';
import axios from 'axios';


class FilterSelect extends Component {

    constructor(props) {
        super(props);
        this.toggleClass= this.toggleClass.bind(this);
        this.state = {
            active: this.props.isSelected,
        };
    }


    async linkFilter(id) {
        const currentFilterId = id;
        const queId = this.props.parentQues;
        const optId = this.props.optId;


        const bodyContent = JSON.stringify({
            questionID:queId,
            filterId: currentFilterId,
            optionId:optId
        });


        const headers = {
            'Content-Type': 'application/json'
        };

        const data = {
            linkObj: bodyContent
        };
        await axios.post('/backend/filter_linked',data, headers );

    }

    async unLinkFilter(id) {
        const currentFilterId = id;
        const queId = this.props.parentQues;
        const optId = this.props.optId;

        const res = await axios.delete(`/backend/filter_linked`,
            {
                params:
                    {
                        questionID:queId,
                        filterId: currentFilterId,
                        optionId:optId
                    }
            });

        // if(res.data){
        //     window.location.reload()
        // }else{
        //     console.log('error updating filter');
        // }

    }

    toggleClass(id) {
        const currentState = this.state.active;
        if(currentState === true) {
            this.unLinkFilter(id);
        } else {
            this.linkFilter(id);
        }
        this.setState({ active: !currentState });
    };


    render() {
        const { item } = this.props;

        return (
            <li className={ this.state.active  ? " Product productActive" : "Product"} key={item._id} onClick={() => this.toggleClass(item._id)}>
                <i  className={this.state.active  ? `Product-icon Product-Icon-Selected fa ${item.browse_icon}` :`Product-icon fa ${item.browse_icon}`}/>
                <h6 className={this.state.active ? "Product-name Product-Name-Selected" : "Product-name"}>
                    {item.name}
                </h6>
            </li>
        );
    }
}

export default FilterSelect;