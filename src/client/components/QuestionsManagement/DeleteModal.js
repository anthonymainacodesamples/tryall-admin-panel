/**
 * Created by AnthonyMaina on 1/15/18.
 */

import React, { Component } from 'react';
import { Modal, Button } from 'react-bootstrap';


class DeleteModal extends Component {
    render() {
        return (
            <Modal
                {...this.props}
                bsSize="small"
                aria-labelledby="contained-modal-title-sm"
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-sm">Are you sure?</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <p>
                        Once deleted, this question will no longer be visible on the on-boarding section of the main Tryall site.
                    </p>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={this.props.onHide} >Cancel </Button>
                    <Button bsStyle="danger" onClick={() => this.props.deleteQuestion()}>Delete</Button>
                </Modal.Footer>
            </Modal>
        );
    }

}
export default DeleteModal;