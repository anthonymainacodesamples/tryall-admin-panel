/**
 * Created by AnthonyMaina on 1/22/18.
 */

import React, { Component } from 'react';
import { PageHeader } from 'react-bootstrap';
import FilterSelect from './FilterSelect';


import _ from 'lodash';

class LinkedFilterSelect extends Component {
    constructor(props) {
        super(props);
        this.state = {
            questionID:"",
            optID:"",
            questionVal:"",
            questionOpt:{},
            completeMain:false,
            completeSelected:false,
            value:{},
            valueCurrent:{}
        };
    }

    componentDidMount() {
        const { questionID,optionID } = this.props.match.params;

        this.getQuestionInfo(questionID,optionID);
        this.fetchFilters();
        this.fetchCurrentSelected(questionID,optionID);

    }

    async fetchFilters() {
        try {
            let response = await fetch('/backend/filters/list');
            const item = await response.json();
            this.setState({
                completeMain:true,
                value: _.mapKeys(item.filterItems, '_id')
            });

            return item;
        } catch (error) {
            console.error(error);
        }

    }

    async getQuestionInfo(qID,oID)  {
        this.setState({
            questionID:qID,
            optionID: oID
        });

        try {
            let response = await fetch(`/backend/retrieve/${qID}/${oID}`);
            const item = await response.json();
            if(item) {
                this.setState({
                    questionVal: item.name,
                    questionOpt: item.option
                });
            }
            return item;
        } catch (error) {
            console.error(error);
        }
    }
    async fetchCurrentSelected(qId,oId) {
        try {
            let response = await fetch(`/backend/filter_linked/${qId}/${oId}`);
            const item = await response.json();
            this.setState({
                completeSelected:true,
                valueCurrent: _.mapKeys(item.value, '_id')
            });

            return item;
        } catch (error) {
            console.error(error);
        }

    }

    renderHeader() {

        const currentOpt = this.state.questionOpt;
        return (
            <PageHeader className="text-left">
                <small>Question: {this.state.questionVal}</small>
                <br />
                <small>Option: { currentOpt.response }</small>
            </PageHeader>
        );
    }
    renderFilters() {

        const totalFil = this.state.value;
        const currentFil = this.state.valueCurrent;
        console.log(totalFil);


        if(this.state.completeMain && this.state.completeSelected) {

            return _.map(totalFil, item => {
                if (currentFil[item._id]) {
                    return (
                        <FilterSelect
                            key={item._id}
                            item={item}
                            isSelected={true}
                            parentQues={this.state.questionID}
                            optId={this.props.match.params.optionID}
                        />
                    );
                } else {
                    return (
                        <FilterSelect
                            key={item._id}
                            item={item}
                            isSelected={false}
                            parentQues={this.state.questionID}
                            optId={this.props.match.params.optionID}/>
                    );
                }
            })
        }

    }

    render() {
        return (
            <div className="container">
                {this.renderHeader()}
                <ul className="ProductList">
                    {this.renderFilters()}
                </ul>
            </div>
        );
    }
}
export default LinkedFilterSelect;
