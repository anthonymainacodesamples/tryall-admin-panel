/**
 * Created by AnthonyMaina on 1/14/18.
 */

import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import {Panel, ListGroup,ListGroupItem, Button} from 'react-bootstrap';
import _ from 'lodash';


// Import custom components
import DeleteModal from './DeleteModal';

class SingleQuestion extends Component {
    constructor(props) {
        super(props);
        this.state = { smShow: false };
        this.deleteQuestion = this.deleteQuestion.bind(this);
    }

    renderButtons() {

        return (
            <div>
                <Link to={`/questions/edit/${this.props.questionID}`} style={{ textDecoration: 'none' }}>
                    <Button bsStyle="primary" >Edit Question</Button>
                </Link>
                {'         '}
                <Button bsStyle="danger" onClick={() => this.setState({ smShow: true })}>Delete Question</Button>
            </div>
        )
    }

    renderItems(list) {
        return _.map(list, item => {
           return (
               <ListGroupItem key={item._id}> {item.response} </ListGroupItem>
           )
        });
    }

    async deleteQuestion() {

        const res = await axios.delete('/backend/remove/question',
            {
                params:
                    {
                        questionID: this.props.questionID
                    }
            });
        if(res.data){
            window.location.reload()
        }else{
            console.log('error updating question');
        }
    }


    returnSingleQuestion() {
        const header = <h3> {this.props.questionPrompt } </h3>;
        return (
            <Panel header={header}  footer={this.renderButtons()} bsStyle="primary" collapsible defaultExpanded>
                <ListGroup>
                    {this.renderItems(this.props.answersList)}
                </ListGroup>
            </Panel>
        );
    }

    render() {
        let smClose = () => this.setState({ smShow: false });

        return(
            <div>
                {this.returnSingleQuestion()}
                <DeleteModal show={this.state.smShow} onHide={smClose} deleteQuestion={this.deleteQuestion}/>
            </div>
        );
    }
}
export default SingleQuestion;