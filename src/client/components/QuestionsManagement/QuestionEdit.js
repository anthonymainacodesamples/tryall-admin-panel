/**
 * Created by AnthonyMaina on 1/14/18.
 */

import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Switch from 'react-switch';
import axios from 'axios';

import _ from 'lodash';
import { PageHeader, Form, FormGroup, FormControl,ControlLabel,Col,HelpBlock,Button } from 'react-bootstrap';

class QuestionEdit extends Component {

    constructor(props){
        super(props);
        this.state = {
            quesID:"",
            questionVal:"",
            questionPoints:0,
            questionOpts:[],
            questionPrompt:"success",
            broadCategories:[],
            newOption:"",
            highestNum:1,
            publishedState:false
        };
        this.change = this.change.bind(this);
        this.changeNew = this.changeNew.bind(this);
        this.deleteOption = this.deleteOption.bind(this);
        this.updateNumberCount = this.updateNumberCount.bind(this);
    }

    change(e) {
        let id = e.target.id;
        switch (id){
            case 'questionPrompt':
                this.setState({
                    questionVal: e.target.value
                });
                break;
            case 'questionPoints':
                this.setState({
                    questionPoints: e.target.value
                });
                break;
            default:
                this.updateSpecificValue(id,e);
                break;
        }
    }

    updateSpecificValue(id,e) {
        const newOptionsArray = this.state.questionOpts;
        let specificOption = _.find(newOptionsArray, function (obj) { return obj._id === id; });
        specificOption.response = e.target.value;
        this.setState({
            questionOpts:newOptionsArray
        });
    }

    async getQuestionInfoFromApi(id)  {
        this.setState({
            quesID:id
        });

        try {
            let response = await fetch(`/backend/questions/${id}`);
            const item = await response.json();
            const obj = item[0];
            if(obj) {
                this.setState({
                    questionVal: obj.prompt,
                    questionPoints:obj.points,
                    questionOpts: obj.options
                });

                this.updateNumberCount(obj.options);
            }
            return item;
        } catch (error) {
            console.error(error);
        }
    }

    updateNumberCount(itemsList) {
        return _.map(itemsList, item => {
            if (item.orderNum > this.state.highestNum) {
                this.setState({
                    highestNum: item.orderNum
                })
            }
        });

    }

    componentDidMount() {
        const { id } = this.props.match.params;
        this.getQuestionInfoFromApi(id);
    }


    renderHeader() {
        return (
            <PageHeader className="text-left">
                <small>Question ID: {this.state.quesID} </small>
                <div className="pull-right">
                    {'  '}
                    <Button bsStyle="primary" type="submit" onClick={this.handleFormSubmit}> Update Question</Button>
                </div>

            </PageHeader>
        );
    }

    handleFormSubmit = (e) => {
        e.preventDefault();
        const body = JSON.stringify({
            questionID:this.state.quesID,
            questionVal: this.state.questionVal,
            questionPoints: this.state.questionPoints,
            published:this.state.publishedState,
            questionOpts: this.state.questionOpts
        });
        this.updateQuestion(body);
    };

    async updateQuestion(bodyContent) {
        console.log(bodyContent);
        const headers = {
            'Content-Type': 'application/json'
        };

        const data = {
            questionObj: JSON.stringify(bodyContent)
        };
        const res = await axios.post('/backend/modify/question',data, headers );
        if(res.data){
            window.location.reload()
        }else{
            console.log('error updating question');
        }

    }

    async appendResponse(bodyContent) {
        const headers = {
            'Content-Type': 'application/json'
        };

        const data = {
            newResponse: JSON.stringify(bodyContent)
        };
        const res = await axios.post('/backend/modify/options',data, headers );
        if(res.data){
            const { id } = this.props.match.params;
            this.getQuestionInfoFromApi(id);
            this.setState({
                newOption:""
            })
        }else{
            console.log('error updating question');
        }

    }

    async deleteOption(queID,optID) {
        const res = await axios.delete('/backend/remove/option',
            {
                params:
                    {
                        questionID: queID,
                        optionID: optID
                    }
            });
        if(res.data){
            const { id } = this.props.match.params;
            this.getQuestionInfoFromApi(id);
        }else{
            console.log('error updating question');
        }
    }

    retrieveLinked(qId,oId) {
        // return fetch(`/backend/linked/${qId}/${oId}`)
        //     .then((response) => response.json())
        //     .then((responseJson) => {
        //     // console.log(responseJson.value);
        //         return responseJson.value;
        //     })
        //     .catch((error) => {
        //         console.error(error);
        //     });
        // axios.get(`/backend/linked/${qId}/${oId}`)
        //     .then(res => {
        //         console.log(res.data.value);
        //         return res.data.value;
        //     });
    }



    renderOptions() {
        return  _.map(this.state.questionOpts,item => {
            return (
                <FormGroup controlId={item._id} key={item._id} validationState={item.response ? "success": "error"}>
                    <Col componentClass={ControlLabel} xs={2}>
                        Option {item.orderNum}
                    </Col>
                    <Col xs={4} className="text-left">
                        <FormControl type="text" value={item.response} onChange={this.change} />
                        <FormControl.Feedback />
                        <HelpBlock>Linked Categories: (Coming soon) </HelpBlock>
                    </Col>
                    <Col xs={6}>
                        <Link to={`/options/${this.state.quesID}/${item._id}`} style={{ textDecoration: 'none' }}>
                            <Button> Linked Categories</Button>
                        </Link>
                        {"   "}
                        <Link to={`/filters/${this.state.quesID}/${item._id}`} style={{ textDecoration: 'none' }}>
                            <Button> Linked Filters</Button>
                        </Link>
                        {"   "}
                        <Button bsStyle={'danger'} onClick={() => this.deleteOption(this.state.quesID,item._id)}> Delete Option</Button>
                    </Col>
                </FormGroup>
            );
        });
    }

    handleNewFormSubmit = (e) => {
        e.preventDefault();
        const body = JSON.stringify({
            questionID:this.state.quesID,
            optionNum: this.state.questionOpts.length ? this.state.highestNum + 1 : 1,
            optionText: this.state.newOption
        });
        this.appendResponse(body);
    };


    changeNew(e) {
        let id = e.target.id;
        switch (id){
            case 'newOpt':
                this.setState({
                    newOption: e.target.value
                });
                break;
            default:
                console.log('in default case');
                break;
        }
    }

    renderCreateNew() {
        return (
            <Form componentClass="fieldset" horizontal>
            <FormGroup controlId="newOpt" validationState="warning">
                <Col componentClass={ControlLabel} xs={2}>
                    New Option { this.state.questionOpts.length ? this.state.highestNum + 1 : 1}
                </Col>
                <Col xs={6}>
                    <FormControl type="text" placeholder="Enter text e.g Akasakasa" value={this.state.newOption} onChange={this.changeNew} validationState={this.state.newOption? "success":"warning"} />
                    <FormControl.Feedback />
                    <HelpBlock>Add it first to edit the linked categories. </HelpBlock>
                </Col>
                <Col xs={4}>
                    <Button type="submit"onClick={this.handleNewFormSubmit}> Add New Option</Button>
                </Col>
            </FormGroup>
            </Form>
        );
    }
    renderForm() {

        return (
        <form>
            <FormGroup className="text-left" controlId="questionPrompt" validationState={this.state.questionVal? "success":"error"}>
                <ControlLabel>Question Prompt</ControlLabel>
                <FormControl type="text" value={this.state.questionVal} onChange={this.change} />
                    <HelpBlock>If this is a new question, type the question prompt then click update question above before adding options.</HelpBlock>
                <FormControl.Feedback />
            </FormGroup>
            <FormGroup className="text-left" controlId="questionPoints" validationState={this.state.questionPoints? "success":"error"}>
                <ControlLabel>Points  (For Matching Algorithm) </ControlLabel>
                <FormControl type="text" value={this.state.questionPoints} onChange={this.change} />
                <HelpBlock>How many points is this question worth.</HelpBlock>
                <FormControl.Feedback />
            </FormGroup>
            <Form componentClass="fieldset" horizontal>
                {this.renderOptions()}
            </Form>
        </form>
        );
    }

    render() {
        return(
            <div className="container">
                {this.renderHeader()}
                {this.renderForm()}
                {this.renderCreateNew()}

            </div>

        );
    }
}

export default QuestionEdit;