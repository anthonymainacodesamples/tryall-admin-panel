/**
 * Created by AnthonyMaina on 1/20/18.
 */

import React, { Component } from 'react';
import SingleFilter from './SingleFilter';
import { Row,Col } from 'react-bootstrap';


class Filters extends Component {

    constructor(props){
        super(props);

        this.getFilters = this.getFilters.bind(this);
        this.saveChecked = this.saveChecked.bind(this);

        this.state = {
            filters: []
        };

    }

    saveChecked = (target) => {

        this.props.checkBoxSelected(target);

        if(target.checked){
            const checkToAvoidDuplicates = this.props.toCheck.includes(target.value);
            if(!checkToAvoidDuplicates) {
                this.props.toCheck.push(target.value)
            }
        }else{
            let index = this.props.toCheck.indexOf(target.value);
            if (index > -1) {
                this.props.toCheck.splice(index, 1)
            }
        }

    };

    componentDidMount(){
        let filters = this.getFilters(this.props);
        this.setState({
            filters: filters
        });
    }

    // Seems to be called every time one types in the tag and desc fields
    // which caused duplicates error
    componentWillReceiveProps(nextProps){
        // console.log(`to check: ${nextProps.toCheck}`);


        let filters = this.getFilters(nextProps);
        // console.log("This was clicked:", target.value);
        // console.log("Array state is:",this.state.categories);
        this.setState({
            filters: filters
        })
    }

    getFilters = (props) => {

        const sortedList = props.list;
        sortedList.sort();
        let bsRowCount = Math.floor(sortedList.length / 3);
        let x = 0;

        let total = [];

        for (let i = 0; i < bsRowCount; i++) {

            let arr = [];

            for (let j = 0; j < 3; j++) {

                let col;

                if (props.toCheck.indexOf(sortedList[x]) > -1) {
                    props.toCheck.push(sortedList[x]);
                    col = (
                        <Col sm={4}>
                            <div className={'pull-left'}>
                                <SingleFilter
                                    checkBoxSelected={this.saveChecked}
                                    value={sortedList[x]}
                                    checked={true}/>
                            </div>

                        </Col>
                    );
                } else {
                    col = (
                        <Col sm={4}>
                            <div className={'pull-left'}>
                                <SingleFilter
                                    checkBoxSelected={this.saveChecked}
                                    value={sortedList[x]}
                                    checked={false}/>
                            </div>

                        </Col>
                    );
                }

                x += 1;
                arr.push(col);
            }

            let row = (
                <Row>
                    {arr}
                </Row>
            );

            total.push(row);

        }

        let remainder = sortedList.length - x;

        let finalArr = [];

        for (let q = 0; q < remainder; q++) {

            let col;

            if (props.toCheck.indexOf(sortedList[x]) > -1) {
                props.toCheck.push(sortedList[x]);
                col = (
                    <Col sm={4}>
                        <div className={'pull-left'}>
                            <SingleFilter
                                checkBoxSelected={this.saveChecked}
                                value={sortedList[x]}
                                checked={true}/>
                        </div>

                    </Col>
                );
            } else {
                col = (
                    <Col sm={4}>
                        <div className={'pull-left'}>
                            <SingleFilter
                                checkBoxSelected={this.saveChecked}
                                value={sortedList[x]}
                                checked={false}/>
                        </div>

                    </Col>
                );
            }
            x += 1;
            finalArr.push(col);
        }

        let finalRow = (
            <Row>
                {finalArr}
            </Row>
        );

        total.push(finalRow);

        return total;
    };


    render() {
        return(
          <div>
              {this.state.filters}
          </div>
        );
    }

}
export default Filters;