import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';
import { Checkbox } from 'react-bootstrap'

class Category extends Component {

    constructor(props){
        super(props);

        this.state = {
            checked: this.props.checked,
            value: this.props.value
        };

        this.handleCheckboxChanged  = this.handleCheckboxChanged.bind(this)
    }

    handleCheckboxChanged = (e) => {
        this.props.checkBoxSelected(e.target)
    };

    render(){
        return(
            <Checkbox
                inline
                defaultChecked={this.state.checked}
                onChange={this.handleCheckboxChanged}
                value={this.state.value}>
                {this.state.value}
            </Checkbox>
        )
    }

}

export default Category