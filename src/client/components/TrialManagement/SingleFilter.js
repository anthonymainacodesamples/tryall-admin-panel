/**
 * Created by AnthonyMaina on 1/20/18.
 */


import React, { Component } from 'react';
import { Checkbox } from 'react-bootstrap'


class SingleFilter extends Component {


    constructor(props){
        super(props);

        this.state = {
            checked: this.props.checked,
            value: this.props.value
        };

        this.handleCheckboxChanged  = this.handleCheckboxChanged.bind(this)
    }

    handleCheckboxChanged = (e) => {
        this.props.checkBoxSelected(e.target)
    };


    render() {
        return (
            <Checkbox
                inline
                defaultChecked={this.state.checked}
                onChange={this.handleCheckboxChanged}
                value={this.state.value}>
                {this.state.value}
            </Checkbox>
        );
    }
}

export default SingleFilter;