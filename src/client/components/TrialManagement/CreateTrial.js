/**
 * Created by AnthonyMaina on 1/17/18.
 */

import React, { Component } from 'react';
import { FormGroup, ControlLabel, FormControl, Form, Col, Row, Button, Modal, HelpBlock, Checkbox, PageHeader} from 'react-bootstrap';
import axios from 'axios';
import _ from 'lodash';
import { Link } from 'react-router-dom';
import Categories from './Categories';
import Filters from './Filters';



class CreateTrial extends Component {

    constructor(props) {
        super(props);
        this.state = {
            randomID:"",
            informationIncomplete: false,
            incompleteInfoOption: '',
            otherSelected: false,
            other: '',
            numberLeftToClean: 0,
            id: '',
            name: '',
            nameState: undefined,
            link: '',
            webLinkState: undefined,
            freeTrialLink: '',
            freeTrialLinkState: undefined,
            logoLink: '',
            logoLinkState: undefined,
            duration: '',
            durationState: undefined,
            conditionalDuration: '',
            conditionalDurationState: undefined,
            price: '',
            priceState: undefined,
            conditionalPricing: '',
            conditionalPricingState: undefined,
            credit_card: '',
            creditCardState: undefined,
            tagline: '',
            tagLineState: undefined,
            desc: '',
            descState: undefined,
            categories: [],
            categoriesState: undefined,
            categoriesList: [],
            selectedFilters:[],
            filterList:[],
            checked: [],
            showModal: false,
            showModalDeleteCompany: false,
            durationDisplay: 'none',
            priceDisplay: 'none'
        };
        this.change = this.change.bind(this);
    }
    componentWillMount(){
        this.fetchCategories();
        this.fetchFilters();
    }
    async fetchFilters() {
        const res = await axios.get('/backend/filters/list');

        const newArr = _.map(res.data.filterItems, ( { name } ) => {
           return name;
        });

        this.setState({
            filterList:newArr
        });
    }

    async fetchCategories() {
        const res = await axios.get('/backend/general/categories');
        this.setState({
            categoriesList:res.data.categoriesList
        });
    }

    change(e){
        let id = e.target.id;
        switch(id){
            case 'formCompanyName':
                this.setState({
                    name: e.target.value,
                    nameState: undefined
                });
                break;
            case 'formWebsiteLink':
                this.setState({
                    link: e.target.value,
                    webLinkState: undefined
                });
                break;
            case 'formFreeTrialLink':
                this.setState({
                    freeTrialLink: e.target.value,
                    freeTrialLinkState: undefined
                });
                break;
            case 'formLogoLink':
                this.setState({
                    logoLink: e.target.value,
                    logoLinkState: undefined
                });
                break;
            case 'formDuration':
                this.setState({
                    duration: e.target.value,
                    durationState: undefined,
                    durationDisplay: 'none'
                });
                break;
            case 'formConditionalDuration':
                this.setState({
                    conditionalDuration: e.target.value,
                    conditionalDurationState: undefined,
                    durationDisplay: 'none'
                });
                break;
            case 'formPrice':
                this.setState({
                    price: e.target.value,
                    priceState: undefined,
                    priceDisplay:'none'
                });
                break;
            case 'formConditionalPrice':
                this.setState({
                    conditionalPricing: e.target.value,
                    conditionalPricingState:undefined,
                    priceDisplay:'none'
                });
                break;
            case 'formCreditCard':
                this.setState({
                    credit_card: e.target.value,
                    creditCardState: undefined
                });
                break;
            case 'formTagLine':
                this.setState({
                    tagline: e.target.value,
                    tagLineState: undefined
                });
                break;
            case 'formDesc':
                this.setState({
                    desc: e.target.value,
                    descState: undefined
                });
                break;
            case 'formCheckbox':
                this.setState({informationIncomplete: e.target.checked});
                break;
            case 'formIncompleteInfo':
                let value = e.target.value;
                if(value !== 'select'){
                    if(value === 'other'){
                        this.setState({otherSelected: true})
                    }else{
                        this.setState({incompleteInfoOption: value});
                        this.setState({otherSelected: false})
                    }
                }
                break;

            case 'other':
                this.setState({other: e.target.value});
                break;

            default:
                console.log('in default case');
                return
        }
    }

    validated = (e) => {

        let state = true;

        let nameCount = this.state.name.length;

        if(nameCount === 0){
            state = false;
            this.setState({
                nameState: 'error'
            });
        }
        let webLinkCount = this.state.link.length;

        if(webLinkCount === 0){
            state = false;
            this.setState({
                webLinkState: 'error'
            });
        }

        let freeTrialLinkCount = this.state.freeTrialLink.length;
        if(freeTrialLinkCount === 0){
            state = false;
            this.setState({
                freeTrialLinkState: 'error'
            });

        }
        let logoLinkCount = this.state.logoLink.length;
        if(logoLinkCount === 0){
            state = false;
            this.setState({
                logoLinkState: 'error'
            });

        }
        let durationCount = this.state.duration.length;
        let conditionalDurationCount = this.state.conditionalDuration.length;

        if(durationCount === 0 && conditionalDurationCount === 0){
            state = false;
            this.setState({
                durationState: 'error',
                conditionalDurationState:'error',
                durationDisplay: 'block'
            });
        }

        let pricingCount = this.state.price.length;

        let conditionalPricingCount = this.state.conditionalPricing.length;

        if(pricingCount === 0 && conditionalPricingCount === 0){
            state = false;
            this.setState({
                priceState: 'error',
                conditionalPricingState:'error',
                priceDisplay: 'block'
            });
        }

        let creditCardCount = this.state.credit_card.length;
        if(creditCardCount === 0){
            state = false;
            this.setState({
                creditCardState: 'error'
            });

        }
        let tagLineCount = this.state.tagline.length;
        if(tagLineCount === 0){
            state = false;
            this.setState({
                tagLineState: 'error'
            });

        }
        let descCount = this.state.desc.length;
        if(descCount === 0){
            state = false;
            this.setState({
                descState: 'error'
            });

        }
        let categoriesCount = this.state.categories.length;
        if(categoriesCount === 0){
            state = false;
            this.setState({
                categoriesState: 'error'
            });

        }

        return state;




    };

    handleCheckboxChanged = (target) => {
        if(target.checked){
            if(this.state.categories.indexOf(target.value) === -1){
                this.state.categories.push(target.value);
            }
        }else{
            if(this.state.categories.indexOf(target.value) > -1){
                let index = this.state.categories.indexOf(target.value);
                this.state.categories.splice(index, 1);
            }
        }

    };

    handleCheckboxFilterChanged = (target) => {

        if(target.checked){
            if(this.state.selectedFilters.indexOf(target.value) === -1){
                this.state.selectedFilters.push(target.value);
            }
        }else{
            if(this.state.selectedFilters.indexOf(target.value) > -1){
                let index = this.state.selectedFilters.indexOf(target.value);
                this.state.selectedFilters.splice(index, 1);
            }
        }

    };

    handleFormSubmit = (e) => {
        e.preventDefault();
        if(this.validated(e)){
            this.setState({
                showModal: true
            })
        }
    };

    handleModalYes = () => {

        this.setState({
            showModal: false
        });

        let other = '';

        if(this.state.informationIncomplete){
            if(this.state.otherSelected){
                other = this.state.other;
            }else{
                other= this.state.incompleteInfoOption;
            }
        }

        let body = '';

        if(other === ''){
            console.log('other is empty');
            body = JSON.stringify({
                name: this.state.name,
                link: this.state.link,
                freeTrialLink: this.state.freeTrialLink,
                logoLink: this.state.logoLink,
                duration: this.state.duration,
                conditionalDuration: this.state.conditionalDuration,
                price: this.state.price,
                conditionalPricing: this.state.conditionalPricing,
                credit_card: this.state.credit_card,
                tagline: this.state.tagline,
                desc: this.state.desc,
                filters:this.state.selectedFilters,
                categories: this.state.categories
            })
        }else{
            body = JSON.stringify({
                name: this.state.name,
                link: this.state.link,
                freeTrialLink: this.state.freeTrialLink,
                logoLink: this.state.logoLink,
                duration: this.state.duration,
                conditionalDuration: this.state.conditionalDuration,
                price: this.state.price,
                conditionalPricing: this.state.conditionalPricing,
                credit_card: this.state.credit_card,
                tagline: this.state.tagline,
                desc: this.state.desc,
                filters:this.state.selectedFilters,
                categories: this.state.categories,
                other: other
            })
        }

        this.createTrial(body);

    };

    async createTrial(bodyContent) {
        const headers = {
            'Content-Type': 'application/json'
        };

        const data = {
            newTrial: bodyContent
        };
        await axios.post('/backend/create/trial',data, headers );

    }

    handleModalClose = () => {
        this.setState({
            showModal: false
        })
    };



    render() {

        return(
          <div >
              <div className="container">
              <PageHeader className="text-left">
                  <small>Create a new trial</small>
              </PageHeader>
              </div>
              <div>
                  <div className={'center-block container'} style={{padding:'10px', width: '80%'}}>
                      <Form horizontal>
                          <FormGroup
                              validationState={this.state.nameState}
                              controlId={'formCompanyName'}>
                              <Col componentClass={ControlLabel} sm={2}>
                                  Company Name:
                              </Col>
                              <Col sm={10}>
                                  <FormControl type={'text'} placeholder={'Enter Company Name'}
                                               value={this.state.name}
                                               onChange={this.change}/>

                              </Col>

                          </FormGroup>
                          <FormGroup
                              validationState={this.state.webLinkState}
                              controlId={'formWebsiteLink'}>
                              <Col componentClass={ControlLabel} sm={2}>
                                  Website Link:
                              </Col>
                              <Col sm={10}>
                                  <FormControl
                                      type={'url'}
                                      placeholder={'Enter Website Link'}
                                      value={this.state.link}
                                      onChange={this.change}/>

                              </Col>
                          </FormGroup>
                          <FormGroup
                              validationState={this.state.freeTrialLinkState}
                              controlId={'formFreeTrialLink'}>
                              <Col componentClass={ControlLabel} sm={2}>
                                  Free Trial Page Link:
                              </Col>
                              <Col sm={10}>
                                  <FormControl
                                      type={'url'}
                                      placeholder={'Enter Free Trial Page Link'}
                                      value={this.state.freeTrialLink}
                                      onChange={this.change}/>

                              </Col>
                          </FormGroup>
                          <FormGroup
                              validationState={this.state.logoLinkState}
                              controlId={'formLogoLink'}>
                              <Col componentClass={ControlLabel} sm={2}>
                                  Logo Link:
                              </Col>
                              <Col sm={10}>
                                  <FormControl
                                      type={'url'}
                                      placeholder={'Enter Logo Link'}
                                      value={this.state.logoLink}
                                      onChange={this.change}/>
                              </Col>
                          </FormGroup>
                          <FormGroup
                              validationState={this.state.durationState}
                              controlId={'formDuration'}>
                              <Col componentClass={ControlLabel} sm={2}>
                                  Duration (Days):
                              </Col>
                              <Col sm={10}>
                                  <FormControl
                                      componentClass={'select'}
                                      value={this.state.duration}
                                      onChange={this.change}>
                                      <option value={'select'}>Select</option>
                                      <option value={'3'}>3</option>
                                      <option value={'7'}>7</option>
                                      <option value={'14'}>14</option>
                                      <option value={'15'}>15</option>
                                      <option value={'21'}>21</option>
                                      <option value={'30'}>30</option>
                                  </FormControl>

                              </Col>
                          </FormGroup>
                          <FormGroup
                              validationState={this.state.conditionalDurationState}
                              controlId={'formConditionalDuration'}>
                              <Col componentClass={ControlLabel} sm={2}>
                                  Conditional Duration (Optional):
                              </Col>
                              <Col sm={10}>
                                  <FormControl
                                      type={'text'}
                                      placeholder={'Enter info for conditional duration'}
                                      value={this.state.conditionalDuration}
                                      onChange={this.change}/>
                                  <HelpBlock style={{display: this.state.durationDisplay}}>If duration is not easily determined, set duration unselected
                                      and describe duration information in the conditional section</HelpBlock>
                              </Col>
                          </FormGroup>
                          <FormGroup
                              validationState={this.state.priceState}
                              controlId={'formPrice'}>
                              <Col componentClass={ControlLabel} sm={2}>
                                  Monthly Price ($):
                              </Col>
                              <Col sm={10}>
                                  <FormControl
                                      type={'number'}
                                      placeholder={'Enter monthly price'}
                                      value={this.state.price}
                                      onChange={this.change}/>
                              </Col>
                          </FormGroup>
                          <FormGroup
                              validationState={this.state.conditionalPricingState}
                              controlId={'formConditionalPrice'}>
                              <Col componentClass={ControlLabel} sm={2}>
                                  Conditional Pricing (Optional):
                              </Col>
                              <Col sm={10}>
                                  <FormControl
                                      type={'text'}
                                      placeholder={'Enter details of conditional pricing'}
                                      value={this.state.conditionalPricing}
                                      onChange={this.change}/>
                                  <HelpBlock style={{display: this.state.priceDisplay}}>If pricing is not easily determined,
                                      set pricing unselected and describe pricing information in the conditional section</HelpBlock>
                              </Col>
                          </FormGroup>
                          <FormGroup
                              validationState={this.state.creditCardState}
                              controlId={'formCreditCard'}>
                              <Col componentClass={ControlLabel} sm={2}>
                                  Requires Credit Card?:
                              </Col>
                              <Col sm={10}>
                                  <FormControl componentClass={'select'} value={this.state.credit_card} onChange={this.change}>
                                      <option value={'select'}>Select</option>
                                      <option value={true}>Yes</option>
                                      <option value={false}>No</option>
                                  </FormControl>
                              </Col>
                          </FormGroup>
                          <FormGroup
                              validationState={this.state.tagLineState}
                              controlId={'formTagLine'}>
                              <Col componentClass={ControlLabel} sm={2}>
                                  Tagline:
                              </Col>
                              <Col sm={10}>
                                  <FormControl componentClass={'textarea'}
                                               rows={2}
                                               value={this.state.tagline}
                                               onChange={this.change}/>
                              </Col>
                          </FormGroup>
                          <FormGroup
                              validationState={this.state.descState}
                              controlId={'formDesc'}>
                              <Col componentClass={ControlLabel} sm={2}>
                                  Description:
                              </Col>
                              <Col sm={10}>
                                  <FormControl componentClass={'textarea'}
                                               rows={5}
                                               value={this.state.desc}
                                               onChange={this.change}/>
                              </Col>
                          </FormGroup>

                          <Checkbox id={'formCheckbox'}
                                    onChange={this.change}
                                    checked={this.state.informationIncomplete}
                                    inline
                                    style={{fontSize: 16, fontWeight: 'bold', verticalAlign: 'middle'}}>
                              Is information incomplete?...
                          </Checkbox>

                          <Col sm={10} smOffset={2}>
                              <FormGroup controlId={'formIncompleteInfo'}
                                         style={{display: this.state.informationIncomplete ? "block" : 'none'}}>
                                  <FormControl componentClass={'select'} onChange={this.change}>
                                      <option value={'select'}>Select...</option>
                                      <option value={'cannot-tell-if-credit-card-needed'}>Cannot tell if credit card is needed</option>
                                      <option value={'pricing-in-a-different-currency'}>Pricing is in a different currency</option>
                                      <option value={'cannot-determine-trial-duration'}>Cannot determine trial duration</option>
                                      <option value={'multiple-pricing-tiers'}>Multiple pricing tiers</option>
                                      <option value={'pricing-not-in--any-currency'}>Pricing not in any currency e.g. point system</option>
                                      <option value={'per-user-pricing'}>Pricing done per user</option>
                                      <option value={'no-logo'}>No logo</option>
                                      <option value={'other'}>Other...</option>
                                  </FormControl>
                              </FormGroup>
                          </Col>

                          <FormGroup controlId={'other'} style={{display: this.state.otherSelected ? 'block' : 'none'}}>
                              <Col sm={2}>
                                  Enter description:
                              </Col>
                              <Col sm={10}>
                                  <FormControl
                                      value={this.state.other}
                                      type={'text'}
                                      placeholder={'Please describe as best as you can'}
                                      onChange={this.change}/>
                              </Col>
                          </FormGroup>

                          <br/>
                          <br/>
                          <FormGroup
                              controlId={'formFilters'}>
                              <Col componentClass={ControlLabel} sm={2}>
                                  Filters:
                              </Col>
                              <Col sm={10}>
                                  <Filters
                                      list={this.state.filterList}
                                      toCheck={this.state.selectedFilters}
                                      checkBoxSelected={this.handleCheckboxFilterChanged}/>
                              </Col>
                          </FormGroup>

                          <FormGroup
                              validationState={this.state.categoriesState}
                              controlId={'formCategories'}>
                              <Col componentClass={ControlLabel} sm={2}>
                                  Categories:
                              </Col>
                              <Col sm={10}>
                                  <Categories
                                      list={this.state.categoriesList}
                                      toCheck={this.state.categories}
                                      checkBoxSelected={this.handleCheckboxChanged}/>
                              </Col>
                          </FormGroup>
                          <FormGroup>
                              <Col>
                                  <Button bsStyle={'primary'} type="submit" onClick={this.handleFormSubmit}>
                                      Add Trial to Database
                                  </Button>
                              </Col>
                              <br/>
                          </FormGroup>
                          <Modal show={this.state.showModal}>
                              <Modal.Body>
                                  <h4>
                                      Are you sure?
                                  </h4>
                              </Modal.Body>
                              <Modal.Footer>
                                  <Row>
                                      <Button onClick={this.handleModalClose}>Cancel</Button>
                                      {' '}
                                      <Link to={'/'} style={{ textDecoration: 'none' }}>
                                        <Button bsStyle={'primary'} onClick={this.handleModalYes}>Yes</Button>
                                      </Link>
                                  </Row>
                              </Modal.Footer>
                          </Modal>
                      </Form>
                  </div>

              </div>
          </div>
        );
    }

}
export default CreateTrial;