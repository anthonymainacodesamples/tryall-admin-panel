import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';
import { Switch, Route } from 'react-router-dom'

//Custom Components
import FindCompany from "./Search/FindCompany";
import CleanData from './TrialManagement/CleanData';
import CategoryManagement from './CategoryManagement/CategoryManagement';
import QuestionManagement from './QuestionsManagement/Questions';
import QuestionEdit from './QuestionsManagement/QuestionEdit';
import CategoryEdit from './CategoryManagement/CategoryEdit';
import CategorySelection from './CategoryManagement/CategorySelectionPerOption';
import FilterSelection from './QuestionsManagement/LinkedFilterSelect';
import CreateTrial from './TrialManagement/CreateTrial';



class Main extends Component {

  render() {

    return (
          <main>
             <Switch>
                <Route exact path='/' component={CleanData}/>
                 <Route exact path='/search' component={FindCompany}/>
                 <Route exact path ='/categories' component={CategoryManagement}/>
                 <Route exact path ='/questions' component={QuestionManagement}/>
                 <Route exact path ='/questions/edit/:id' component={QuestionEdit}/>
                 <Route exact path ='/categories/edit/:id' component={CategoryEdit}/>
                 <Route exact path ='/new/trial' component={CreateTrial} />
                 <Route exact path ='/options/:questionID/:optionID'component={CategorySelection} />
                 <Route exact path ='/filters/:questionID/:optionID'component={FilterSelection} />
            </Switch>
          </main>
    );
  }
}

export default Main;
