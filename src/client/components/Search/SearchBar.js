import React, { Component } from 'react'
import Autosuggest from 'react-autosuggest'
import '../../styles/SearchBar.css'



// When suggestion is clicked, Autosuggest needs to populate the input
// based on the clicked suggestion. Teach Autosuggest how to calculate the
// input value for every given suggestion.
const getSuggestionValue = suggestion => suggestion.name;

// Use your imagination to render suggestions.
const renderSuggestion = suggestion => (
  <div className={'list-group-item'}>
    {suggestion.name}
  </div>
);

function renderSuggestionsContainer({ containerProps , children, query }) {
  return (
    <div {... containerProps}>
        <ul className={'list-group'}>
          {children}
        </ul>
    </div>
  );
}

const renderInputComponent = inputProps => (
  <form className={'input-group'}>
    <input className={'form-control'} {...inputProps} />
    <span className="input-group-btn">
        <button type="submit" className="btn btn-secondary"> Search</button>
    </span>
  </form>
);

class SearchBar extends Component{


    constructor(){
        super();

        this.state = {
            value: '',
            fetched: [],
            suggestions: [],
            name: '',
            link: '',
            duration: '',
            price: '',
            credit_card: '',
            tagline: '',
            desc: ''
        }

    }

    // Teach Autosuggest how to calculate suggestions for any given input value.
    getSuggestions = value => {

      const inputValue = value.trim().toLowerCase();
      const inputLength = inputValue.length;

      return inputLength === 0 ? [] : this.state.fetched.filter(item =>
        item.name.toLowerCase().slice(0, inputLength) === inputValue
      );
    };


    async getElasticSearchSuggestions(value) {
        const inputValue = value.trim().toLowerCase();
        fetch(`/test/${inputValue}`)
            .then((response) => {
                console.log(response);
                return response.json();
            })
            .then((response) => {
                console.log(response);
                return response;
            })
            .catch((err) => console.log(err))
    };

    onChange = (event, { newValue }) => {
        this.setState({
          value: newValue
        });
    };

  // Autosuggest will call this function every time you need to update suggestions.
  // You already implemented this logic above, so just use it.
      onSuggestionsFetchRequested = ({ value }) => {
          const inputValue = value.trim().toLowerCase();
        fetch(`/test/${inputValue}`)
            .then((response) => {
                return response.json();
            })
            .then((response) => {
                this.setState({
                    suggestions: response
                });
            })
            .catch((err) => console.log(err));
      };

  // Autosuggest will call this function every time you need to clear suggestions.
      onSuggestionsClearRequested = () => {
        this.setState({
          suggestions: []
        });
      };

      onSuggestionSelected = (event, { suggestion, suggestionValue, suggestionIndex, sectionIndex, method }) => {
          console.log(suggestion);
          fetch(`/get/${suggestion.id}`)
            .then((response) => {
                console.log(response);
                return response.json();
            })
            .then((response) => {
                this.props.onSelectSuggestion(response);

            })
            .catch((err) => console.log(err))
        };

    componentDidMount(){
        fetch('/elastic')
            .then((response) => {
                console.log(response);
                return response.json();
            })
            .then((response) => {
                console.log(response);
                this.setState({
                    fetched: response.items
                })
            })
            .catch((err) => console.log(err))
    }


    render(){

        const { value, suggestions } = this.state;

    // Autosuggest will pass through all these props to the input.
        const inputProps = {
            className: 'form-control',
          placeholder: 'Type search term and press enter',
          value,
          onChange: this.onChange
        };

        return (
            <div className="row col-md-6 col-md-offset-3">

                <Autosuggest
                suggestions={suggestions}
                renderSuggestionsContainer={renderSuggestionsContainer}
                renderInputComponent={renderInputComponent}
                onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                onSuggestionSelected={this.onSuggestionSelected}
                getSuggestionValue={getSuggestionValue}
                renderSuggestion={renderSuggestion}
                inputProps={inputProps}
                />

            </div>
        )
    };

}

export default SearchBar