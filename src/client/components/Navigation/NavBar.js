import React from 'react'
import { Navbar, NavItem, Nav } from 'react-bootstrap'
import { LinkContainer } from 'react-router-bootstrap' // https://stackoverflow.com/questions/35687353/react-bootstrap-link-item-in-a-navitem

const Header = () => (

    <Navbar inverse collapseOnSelect>
        <Navbar.Header>
            <Navbar.Brand>
                Tryall Company Data Manager
            </Navbar.Brand>
            <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
      <Nav>
          <LinkContainer to={'/'}>
             <NavItem eventKey={1}>Clean Data</NavItem>
          </LinkContainer>
          <LinkContainer to={'/search'}>
             <NavItem eventKey={2}>Search</NavItem>
          </LinkContainer>
          <LinkContainer to={'/questions'}>
              <NavItem eventKey={2}>Questions</NavItem>
          </LinkContainer>
          <LinkContainer to={'/categories'}>
              <NavItem eventKey={2}>Categories</NavItem>
          </LinkContainer>
      </Nav>
    </Navbar.Collapse>
    </Navbar>

);

export default Header