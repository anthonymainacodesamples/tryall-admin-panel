from pymongo import MongoClient

client = MongoClient('mongodb://admin:FM9-SMY-wZe-kZw@34.228.18.70:27017')
db = client.tryall
categories = db.categories
broad = db.broad_categories

arr = [
    {
        "name": "Advertising & Marketing",
        "sub_categories": [
            "Advertising Networks",
            "Advertising Platforms",
            "Ad Targeting",
            "Brand Marketing",
            "Direct Advertising",
            "Email Marketing",
            "Market Research",
            "Marketing Automation",
            "Social Media Advertising",
            "Social Media Marketing",
        ]},
    {
        "name": "Analytics",
        "sub_categories": [
            "Business Analytics",
            "Social Media Analytics",
            "Text Analytics",
        ]},
    {
        "name": "Mobile Apps",
        "sub_categories": [
            "Android",
            "Mobile Software Tools",
            "iOS"
        ]},
    {
        "name": "Beauty",
        "sub_categories": []},
    {
        "name": "Big Data",
        "sub_categories": [
            "Big Data Analytics"

        ]},
    {
        "name": "Biotech",
        "sub_categories": []},
    {
        "name": "Blogging",
        "sub_categories": []},
    {
        "name": "Books",
        "sub_categories": [
            "Audiobooks"
        ]},
    {
        "name": "Business Software",
        "sub_categories": [
            "Business Development",
            "Business Intelligence",
            "Business Management",
            "Business Productivity",
            "Digital Signage",
            "Employee Management",
            "Enterprise Search",
            "Event Management",
            "Fleet Management",
            "Human Resources",
            "Lead Generation",
            "Logistics",
            "Product Management",
            "Project Management",
            "Recruiting",
            "Risk Management",
            "SEO",
            "Sales",
            "Search",
            "Social Business",
            "Team Management",
            "Video Conferencing"
        ]},
    {
        "name": "CRM",
        "sub_categories": [
            "Customer Acquisition",
            "Customer Management",
            "Customer Support Tools",
            "Social CRM"

        ]},
    {
        "name": "Career Planning",
        "sub_categories": []},
    {
        "name": "Communication / Messaging",
        "sub_categories": [
            "Email"
        ]},
    {
        "name": "Engineering",
        "sub_categories": [
            "Civil Engineers",
            "Innovation Engineering"
        ]},
    {
        "name": "Cloud Computing",
        "sub_categories": [
            "Cloud Security"
        ]},
    {
        "name": "Hardware",
        "sub_categories": [
            "Communications Hardware"
        ]},
    {
        "name": "Community Development",
        "sub_categories": []},
    {
        "name": "Computer Vision",
        "sub_categories": []},
    {
        "name": "Consumer Electronics",
        "sub_categories": []},
    {
        "name": "Deals",
        "sub_categories": [
            "Discounts",
            "Coupons",
            "Loyalty Programs",
        ]},
    {
        "name": "Cyber Security",
        "sub_categories": [
            "Data Security",
            "Network Security",
        ]},
    {
        "name": "Data Analysis",
        "sub_categories": []},
    {
        "name": "Data Visualization",
        "sub_categories": []},
    {
        "name": "Databases",
        "sub_categories": []},
    {
        "name": "Design",
        "sub_categories": [
            "Web Design"
        ]},
    {
        "name": "Developer APIs",
        "sub_categories": []},
    {
        "name": "Digital Media",
        "sub_categories": [
            "Digital Magazines"
        ]},
    {
        "name": "Domains",
        "sub_categories": []},
    {
        "name": "E-Commerce",
        "sub_categories": [
            "Mobile Commerce",
            "Social Commerce"
        ]},
    {
        "name": "EdTech",
        "sub_categories": [
            "Language Learning"
        ]},
    {
        "name": "Entrepreneurship",
        "sub_categories": []},
    {
        "name": "Finance",
        "sub_categories": [
            "Estimation and Quoting",
            "Hedge Funds",
            "Mobile Payments",
            "Stock Exchanges",
            "Trading",
            "Accounting"
        ]},
    {
        "name": "Fashion",
        "sub_categories": []},
    {
        "name": "File Sharing",
        "sub_categories": [
            "Photo Sharing"
        ]},
    {
        "name": "Freemium",
        "sub_categories": []},
    {
        "name": "Games",
        "sub_categories": [
            "Mobile Games"
        ]},
    {
        "name": "Health & Wellness",
        "sub_categories": [
            "Health Diagnostics",
            "Medical Devices",
            "Medical Professionals"
        ]},
    {
        "name": "Hotels",
        "sub_categories": []},
    {
        "name": "Internet of Things",
        "sub_categories": []},
    {
        "name": "Legal",
        "sub_categories": []},
    {
        "name": "Location Based Services",
        "sub_categories": []},
    {
        "name": "Machine Learning",
        "sub_categories": [
            "Natural Language Processing",
            "Predictive Analytics"
        ]},
    {
        "name": "Media & Entertainment",
        "sub_categories": [
            "Movies and Series",
            "Music",
            "Video Chat"
        ]},
    {
        "name": "Networking",
        "sub_categories": []},
    {
        "name": "News",
        "sub_categories": []},
    {
        "name": "Online Rental",
        "sub_categories": []},
    {
        "name": "Travel & Tourism",
        "sub_categories": [
            "Outdoors",
            "Travel",
            "Adventure Travel"
        ]},
    {
        "name": "Open Source",
        "sub_categories": []},
    {
        "name": "Personal Development",
        "sub_categories": []},
    {
        "name": "Pets",
        "sub_categories": []},
    {
        "name": "Photography",
        "sub_categories": []},
    {
        "name": "Presentations",
        "sub_categories": []},
    {
        "name": "Productivity",
        "sub_categories": []},
    {
        "name": "Property Management",
        "sub_categories": [
            "Real Estate"
        ]},
    {
        "name": "Psychology",
        "sub_categories": []},
    {
        "name": "Publishing",
        "sub_categories": []},
    {
        "name": "Startups",
        "sub_categories": []},
    {
        "name": "Retail",
        "sub_categories": []},
    {
        "name": "Reviews and Recommendations",
        "sub_categories": []},
    {
        "name": "Robotics",
        "sub_categories": []},
    {
        "name": "Small and Medium Businesses",
        "sub_categories": []},
    {
        "name": "Social Innovation",
        "sub_categories": []},
    {
        "name": "Social Media",
        "sub_categories": [
            "Social Media Management",
            "Social Media Analytics"
        ]},
    {
        "name": "Sports",
        "sub_categories": []},
    {
        "name": "Utilities",
        "sub_categories": []},
    {
        "name": "Virtualization",
        "sub_categories": [
            "Augmented Reality",
            "Virtual Reality"
        ]}
]

# res = broad.insert_many(arr)
# print(res.inserted_ids)
# print(db.broad_categories.count())

for i in arr:
    # res = broad.insert_one(i)

    # print(i['name'])

    broad.insert_one({
        "name": i["name"],
        "sub_categories": i['sub_categories']
    })

print(db.broad_categories.count())

# print(db.broad_categories.count())
# print(categories.find())

# with open('categories_sorted.txt') as f:
#     for i in f:
#         js = {}
#         js['value'] = i.strip()
#         arr.append(js)
#
#
# print(arr)
# res = categories.insert_many(arr)
# print(res)

# for i in range(len(arr)):
#     result = categories.insert_one(arr[i])
#     print(result)
# result = categories.insert_many(arr)
# print(result.inserted_ids)
