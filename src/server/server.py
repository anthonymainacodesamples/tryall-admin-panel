from flask import Flask
from flask import request
import json
from pymongo import MongoClient
from pymongo import ReturnDocument
from elasticsearch import Elasticsearch
from bson.objectid import ObjectId
from random import randint

app = Flask(__name__)

client = MongoClient('mongodb://admin:FM9-SMY-wZe-kZw@174.129.68.131:27017')
db = client.tryall
companies = db.tryallcompanies
categories_db = db.categories
filters_db = db.filters


categories = []
filters= []

filtersFind = filters_db.find()
for i in filtersFind:
    filters.append(i['name'])

categoriesFind = categories_db.find()
for i in categoriesFind:
    categories.append(i['value'])


# es = Elasticsearch(['http://nginx:@34.224.75.114:80'])

es = Elasticsearch(
    ['34.224.75.114'],
    http_auth=('nginx', "61h5BvMih111FKC"),
    scheme="http",
    port=80,
)


@app.route('/api')
def hello_world():
    return json.dumps({
        'name': 'hello world!',
        'link': 'https://www.w3schools.com/tags/att_input_type.asp',
        'duration': 7,
        'price': 30,
        'credit_card': False,
        'tagline': 'Use flex in a component\'s style to have the component expand and shrink',
        'desc': "Use flex in a component's style to have the component expand and shrink dynamically based on "
                "available space. Normally you will use flex: 1, which tells a component to fill all available space, "
                "shared evenly amongst each other component with the same parent. The larger the flex given, "
                "the higher the ratio of space a component will take compared to its siblings."

    })


@app.route('/elastic')
def elastic():
    return json.dumps({
        "items": [
            {
                "name": "Anthony"
            },
            {
                "name": "Jason"
            },
            {
                "name": "Gabriel"
            }
        ]
    })


@app.route('/get/<id>')
def get_Stuff(id):
    res = companies.find_one({"_id": ObjectId(id)})
    print("get result: {0}".format(res))

    if "websiteLink" in res:
        websiteLink = res['websiteLink']
    else:
        websiteLink = ""

    if "freeTrialPage" in res:
        freeTrialPage = res['freeTrialPage']
    else:
        freeTrialPage = ""

    if "logoLink" in res:
        logoLink = res['logoLink']
    else:
        logoLink = ""

    if "DurationInDays" in res:
        duration = res['DurationInDays']
    else:
        duration = 0

    if "conditionalDuration" in res:
        durationCondition = res['conditionalDuration']
    else:
        durationCondition = ""

    if "monthlyDollarPrice" in res:
        price = res['monthlyDollarPrice']
    else:
        price = ""

    if "conditionalPricing" in res:
        priceCondition = res['conditionalPricing']
    else:
        priceCondition = ""

    if "requiresCreditCard" in res:
        credit = res['requiresCreditCard']
    else:
        credit = ""

    if "tagLine" in res:
        tagLine = res['tagLine']
    else:
        tagLine = ""

    if "description" in res:
        desc = res["description"]
    else:
        desc = ""

    categories = []

    categoriesFind = categories_db.find()
    for i in categoriesFind:
        categories.append(i['value'])

    filters = []

    filtersFind = filters_db.find()
    for i in filtersFind:
        filters.append(i['name'])


    return json.dumps({
        'id': id,
        'name': res['name'],
        'link': websiteLink,
        'freeTrialPage': freeTrialPage,
        'logoLink': logoLink,
        'duration': duration,
        'conditionalDuration': durationCondition,
        'price': price,
        'conditionalPricing': priceCondition,
        'credit_card': credit,
        'tagline': tagLine,
        'desc': desc,
        'filters-list':filters,
        #'filters': res['filters'],
        'categories-list': categories,
        'categories': res['categories']
    })


def hasRevisited(revisited, random_record):
    for i in revisited:
        if random_record["_id"] == i["_id"]:
            return True


@app.route('/get-dirty-data')
def get_dirty_data():
    vals = db.revisit.find()

    res = companies.find({
        '$or': [
            {'websiteLink': ''},
            {'logoLink': ''},
            {'tagLine': ''},
            {'description': ''},
            {'categories': {'$size': 0}}

        ]
    })

    random_record = res.__getitem__(randint(0, res.count() - 1))

    while hasRevisited(vals, random_record):
        random_record = res.__getitem__(randint(0, res.count() - 1))

    print('random: {0}'.format(random_record))

    if "websiteLink" in random_record:
        websiteLink = random_record['websiteLink']
    else:
        websiteLink = ""

    if "freeTrialPage" in random_record:
        freeTrialPage = random_record['freeTrialPage']
    else:
        freeTrialPage = ""

    if "logoLink" in random_record:
        logoLink = random_record['logoLink']
    else:
        logoLink = ""

    if "DurationInDays" in random_record:
        duration = random_record['DurationInDays']
    else:
        duration = 0

    if "conditionalDuration" in random_record:
        durationCondition = random_record['conditionalDuration']
    else:
        durationCondition = ""

    if "monthlyDollarPrice" in random_record:
        price = random_record['monthlyDollarPrice']
    else:
        price = ""

    if "conditionalPricing" in random_record:
        priceCondition = random_record['conditionalPricing']
    else:
        priceCondition = ""

    if "requiresCreditCard" in random_record:
        credit = random_record['requiresCreditCard']
    else:
        credit = ""

    if "tagLine" in random_record:
        tagLine = random_record['tagLine']
    else:
        tagLine = ""

    if "description" in random_record:
        desc = random_record["description"]
    else:
        desc = ""

    categories = []

    categoriesFind = categories_db.find()
    for i in categoriesFind:
        categories.append(i['value'])

    filters = []

    filtersFind = filters_db.find()
    for i in filtersFind:
        filters.append(i['name'])

    to_return = {
        'id': str(random_record['_id']),
        'name': random_record['name'],
        'link': websiteLink,
        'freeTrialPage': freeTrialPage,
        'logoLink': logoLink,
        'duration': duration,
        'conditionalDuration': durationCondition,
        'price': price,
        'conditionalPricing': priceCondition,
        'credit_card': credit,
        'tagline': tagLine,
        "desc": desc,
        #'filters':random_record['filters'],
        'filters-list' : filters,
        'categories': random_record['categories'],
        'categories-list': categories
    }

    return json.dumps({
        'numberLeftToClean': res.count(),
        'randomRecord': to_return
    })


@app.route('/update/<id>', methods=['POST'])
def update(id):
    updates = request.data.decode('utf-8')
    updates = json.loads(updates)

    print(updates)

    if 'other' in updates:
        revisit = db.revisit

        res = revisit.insert_one(
            {"tryallId": ObjectId(id)},
            {'other': updates['other']}
        )



        print('res: {0}'.format(res))
    else:
        res = companies.find_one_and_update(
            {"_id": ObjectId(id)},
            {'$set': {
                'name': updates['name'],
                'websiteLink': updates['link'],
                'freeTrialPage': updates['freeTrialLink'],
                'logoLink': updates['logoLink'],
                'DurationInDays': updates['duration'],
                'conditionalDuration': updates['conditionalDuration'],
                'monthlyDollarPrice': updates['price'],
                'conditionalPricing': updates['conditionalPricing'],
                'requiresCreditCard': updates['credit_card'],
                'tagLine': updates['tagline'],
                "description": updates["desc"],
                'filters':updates['filters'],
                'categories': updates['categories']
            }},
            return_document=ReturnDocument.AFTER
        )



        # re-index in elasticSearch
        doc = {
            "doc": {
                'name': updates['name'],
                'websiteLink': updates['link'],
                'freeTrialPage': updates['freeTrialLink'],
                'logoLink': updates['logoLink'],
                'DurationInDays': updates['duration'],
                'conditionalDuration': updates['conditionalDuration'],
                'monthlyDollarPrice': updates['price'],
                'conditionalPricing': updates['conditionalPricing'],
                'requiresCreditCard': updates['credit_card'],
                'tagLine': updates['tagline'],
                "description": updates["desc"],
                'filters': updates['filters'],
                'categories': updates['categories']
            }
        }
        es_update_res = es.update(index="tryallcompanys", doc_type="tryallcompany", id=id, _source=True, body=doc)
        # print(es_update_res)

        print('res: {0}'.format(res))

    return json.dumps({'success': True})


@app.route('/test/<search_term>')
def test_elastic(search_term):
    # query = {
    #     "from": 0,
    #     "size": 10,
    #     "query": {
    #         "bool": {
    #             "should": [
    #                 {
    #                     "match": {
    #                         "_all": search_term
    #                     }
    #                 },
    #                 {
    #                     "match": {
    #                         "_all": {
    #                             "query": search_term,
    #                             "fuzziness": "1",
    #                             "prefix_length": 2,
    #                             "operator": "and"
    #                         }
    #                     }
    #                 }
    #             ]
    #         }
    #     }
    # }

    query = {
        "from": 0,
        "size": 15,
        "query": {
            'query_string': {
                'query': "**" + search_term + "*",
                'fields': ['name^2', 'categories']
            }
        }
    }

    response = es.search(index="tryallcompanys", body=query)
    # print(response)
    results = []
    for hit in response['hits']['hits']:
        # print(hit)
        res = {}
        res['name'] = hit['_source']['name']
        res['id'] = hit['_id']
        results.append(res)
    print(results)

    return json.dumps(results)


@app.route('/delete/<company_id>')
def delete_company(company_id):
    res = companies.delete_one({"_id": ObjectId(company_id)})

    if res.acknowledged:
        return json.dumps({'success': True})
    else:
        return json.dumps({'success': False})


if __name__ == '__main__':
    app.run()
